
import Browser
import ViewData exposing (showMaybe, showList)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


----------------------------------------------------------------------
----------------------------------------------------------------------
--             Levi, ignore this section

type Msg
  = ButtonClick


main =
  Browser.sandbox { init = init, update = update, view = view }


update : Msg -> Model -> Model
update msg model = 
  case msg of
    ButtonClick -> { model | result = operate model.result }


view : Model -> Html Msg
view model =
  div [] 
    [ display model
    , input 
      
        [ type_ "button", value "a button", onClick ButtonClick ] 
      
        []
    ]

----------------------------------------------------------------------
----------------------------------------------------------------------


----------------------------------------------------------------------
-- This is the first time we will introduce algebraic data types.  Suppose you
-- have a variable call 'vehicle' that will specify some kind of vehicle along
-- with its price. So the *type* is Vehicle. Notice that trucks don't have
-- prices in this program, let's say because they are provided free by the
-- government.

type alias Price = Float

type Vehicle = BoatVehicle Price
             | TruckVehicle
             | CarVehicle Price


-- Here's a variable 'vehicle1' that specifies a boat. Notice that the type
-- signature is simply 'Vehicle', but the *specific* vehicle is a boat. The
-- price is 1 million dollars.
vehicle1 : Vehicle
vehicle1 = BoatVehicle 1000000


-- Here's a variable 'vehicle2' that specifies a truck. No price is needed.
vehicle2 : Vehicle
vehicle2 = TruckVehicle


-- We need to define a way to "show" Vehicle types (convert them to a string).

showVehicle v = case v of
  BoatVehicle price -> "BoatVehicle (" ++ String.fromFloat price ++ ")"
  TruckVehicle      -> "TruckVehicle"
  CarVehicle price  -> "CarVehicle (" ++ String.fromFloat price ++ ")"

----------------------------------------------------------------------
-- STEP 1: INTIALIZE some test data in a variable with a name like 
--         testData01, testData02, etc.
----------------------------------------------------------------------

-- FOR TEST DATA IN THIS FILE, we'll use 'vehicle1'

----------------------------------------------------------------------
-- STEP 2: GIVE 'result' a TYPE SIGNATURE the same as your test data.
----------------------------------------------------------------------
type alias Model =
  { result      : Vehicle
  }


----------------------------------------------------------------------
-- STEP 3: INITIALIZE the 'result' field to the TEST DATA VARIABLE that
--         you have chosen.
----------------------------------------------------------------------
init : Model
init =
  { result = vehicle1 }


----------------------------------------------------------------------
-- STEP 4: define a function for OPERATING on your test data.
--
-- For this file, we'll change the vehicle to a truck.
--
----------------------------------------------------------------------
operate : Vehicle -> Vehicle
operate v = case v of
  TruckVehicle    -> BoatVehicle 100
  BoatVehicle p   -> CarVehicle p
  CarVehicle _    -> TruckVehicle


----------------------------------------------------------------------
-- STEP 5: PUT the correct function at the location below for SHOWING the
--         'result' field (that means CONVERTING to a string).
--
----------------------------------------------------------------------
display : Model -> Html Msg
display model = 
  let 
      --------------------------------------------------
      -- PUT the correct function here 
      --------------------------------------------------
      s = showVehicle model.result
  in 
      div [] [text s]




