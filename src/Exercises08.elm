

-- Exercises in function composition

import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)


main =
  Html.text <| Debug.toString output

----------------------------------------------------------------------
----------------------------------------------------------------------
-- TYPE to use

-- (1) Here's a data type with three cases. It describes an appliance

type alias Price = Float

type alias LowTemperature = Float

type alias HighTemperature = Float

type alias FoodName = String

type alias DishName = String

type alias AmountSoap = Float

type Appliance = Fridge Price LowTemperature (List FoodName)
               | Oven Price HighTemperature FoodName
               | Dishwasher Price AmountSoap (List DishName)

----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: describe this data type in English. Write a description of each
-- case, describe the data that goes with that case, and describe why the data
-- is appropriate for that case.

-- ... write here ...


----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: create test data. Make some individual appliances, then put
-- them into a list as well.
--
--


----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: write a function of type "Appliance -> Maybe a" that picks a
-- data field out of the type for some cases but not others.

-- this is an example for a List of food names. You pick some other field.
getFoodList : Appliance -> Maybe (List FoodName)
getFoodList appl =
  case appl of
      Fridge _ _ foodList     -> Just foodList
      Dishwasher _ _ foodList -> Just foodList
      Oven _ _ _              -> Nothing


----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: write function that can be used for filter.

-- (1) write a function that gets the price 
--
-- (2) write a function that checks if the price is above or below a certain
-- cutoff
--
-- (3) write an expression that gets all the appliances in the test data list
-- that are picked by the filter function
--

----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: write two more filter functions. Start by picking some field,
-- pick some other variable like the number of food items and write filter
-- functions.




