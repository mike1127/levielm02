
import Browser
import ViewData exposing (showMaybe, showList)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)



----------------------------------------------------------------------
-- INTRODUCTION: 
--
-- This is the first time we will introduce mapping a function over a list. 


----------------------------------------------------------------------
-- STEP 1: INTIALIZE some test data in a variable with a name like 
--         testData01, testData02, etc.
----------------------------------------------------------------------

inputA : List Int
inputA = [5, 6, 7, 8, 9, 10]


inputB : String
inputB = "bread"


goal01 : String
goal01 = "56789"

goal02 : String
goal02 = "10,9,8,7,6,5"


goal03 : String
goal03 = "read"


goal04 : String
goal04 = "daerbdaerb"


operate01 : List Int -> String
operate01 inp =
  let
    l2 = List.filter (\x -> x /= 10) inp
  in
    String.join "" <| List.map String.fromInt l2


operate02 : List Int -> String
operate02 inp
  String.join "," <| List.map String.fromInt <| List.reverse


-- FOR TEST DATA IN THIS FILE, we'll use 'vehicle1'

----------------------------------------------------------------------
-- STEP 2: GIVE 'result' a TYPE SIGNATURE the same as your test data.
----------------------------------------------------------------------
type alias Model =
  { result      : String
  }


----------------------------------------------------------------------
-- STEP 3: INITIALIZE the 'result' field to the TEST DATA VARIABLE that
--         you have chosen.
----------------------------------------------------------------------
init : Model
init =
  { result = operate02 inputA }


----------------------------------------------------------------------
-- STEP 4: define a function for OPERATING on your test data.
--
-- For this file, we'll change the vehicle to a truck.
--
----------------------------------------------------------------------

someFunction : Int -> Int
someFunction x = x * x

operate : String -> String
operate l = l


----------------------------------------------------------------------
-- STEP 5: PUT the correct function at the location below for SHOWING the
--         'result' field (that means CONVERTING to a string).
--
----------------------------------------------------------------------
display : Model -> Html Msg
display model = 
  let 
      --------------------------------------------------
      -- PUT the correct function here 
      --------------------------------------------------
      s = model.result
  in 
      div [] [text s]




----------------------------------------------------------------------
----------------------------------------------------------------------
--             ignore this section

type Msg
  = ButtonClick


main =
  Browser.sandbox { init = init, update = update, view = view }


update : Msg -> Model -> Model
update msg model = 
  case msg of
    ButtonClick -> { model | result = operate model.result }


view : Model -> Html Msg
view model =
  div [] 
    [ display model
    , input 
      
        [ type_ "button", value "a button", onClick ButtonClick ] 
      
        []
    ]

----------------------------------------------------------------------
----------------------------------------------------------------------

