--
--      test if string is a palindrome
--

import Browser
import ViewData exposing (showMaybe, showList)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


showBool : Bool -> String
showBool b = 
  if b
    then "True"
    else "False"


operate : String -> String -> String
operate inp1 _ = showBool <| inp1 == String.reverse inp1

----------------------------------------------------------------------

type alias Model =
  { field1      : String
  , field2      : String
  , result      : String
  }


type Msg
  = Change1 String
  | Change2 String


main =
  Browser.sandbox { init = init, update = update, view = view }


init : Model
init =
  { field1 = ""
  , field2 = ""
  , result = "" }


update : Msg -> Model -> Model
update msg model = 
  let 
    model2 = case msg of
      Change1 s -> { model | field1 = s }
      Change2 s -> { model | field2 = s }
  in
    { model2 | result = operate model2.field1 model2.field2 }


view : Model -> Html Msg
view model =
  div []
    [ input [ value model.field1, onInput Change1 ] []
    , input [ value model.field2, onInput Change2 ] []
    , div [] [ text model.result ]
    ]

