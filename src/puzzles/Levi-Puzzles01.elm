
import Debug
import List.Extra exposing (setAt, getAt)
import Html exposing (text)

main = output |> Debug.toString |> text


----------------------------------------------------------------------
-- we're using the function 'setAt' from List.Extra. Guess what this does. (I
-- made my own function for doing this, but I discovered a standard library
-- that provides it, so that's better for us.)

output01 =
  let
      d = [ 'a', 'b', 'c', 'd' ]
  in
      setAt 2 'e' d

-- setAt replaces the 3rd item in the list (d) from a 'c' to an 'e'

-- UNCOMMENT to test:
-- output = output01

----------------------------------------------------------------------
-- here we make a variation on 'setAt' that takes a tuple

setAtTuple : (Int, a) -> List a -> List a
setAtTuple (idx, value) input = setAt idx value input

-- ASSIGNMENT: write 2 or 3 examples of using setAtTuple with lists of
-- different types

outputLevi01A =
  let
    listA = List.range 1 10
  in
    setAtTuple (1, 10) listA


outputLevi01B =
  let
    classroomList = [ (14, "Jeremy"), (16, "Maggie")
                    , (15, "Elijah"), (15, "Harper") ]
  in
    setAtTuple (1, (17, "James")) classroomList

-- output = outputLevi01A



----------------------------------------------------------------------
-- here we use 'getAt' from List.Extra. Look up the documentation (search for
-- "List.Extra elm") and see what it does. Make sure you understand its type
-- signature.

output02 = getAt 0 [ "George", "Mary", "Peter", "Paul" ]

-- output = output02


----------------------------------------------------------------------
-- ASSIGNMENT: write a function as follows:
--
--   move3to2 : List a -> List a
--
-- that takes a list 'input', then gets the value at index 3 ('index3Value')
-- and sets the position of the list at index 2 to 'index3Value'.
--
-- Handle the case that there's no value at index 3 or 2 (i.e the list is too
-- short). Come up with sensible behavior for these cases.

move3to2 : List a -> List a
move3to2 ls =
  case getAt 2 ls of
    Nothing ->
      ls
    Just a ->
      setAt 1 a ls

-- output = move3to2 [1, 2]

----------------------------------------------------------------------
--
-- here's an introduction to the function concat

output03 = List.concat [ [1, 3], [2, 5], [7], [8, 9, 10] ]

-- output = output03


----------------------------------------------------------------------
--
--
-- Here's a puzzle. we want to take the string
--    "txyw" and produce "ttxxyyww"
--
--    "abc" and produce "aabbcc"
--
--  See the pattern?

-- for all of the following uncomment each assignment of 'output', one at a
-- time, and see what it does.

-- ASSIGNMENT: write one sentence for each test variable explaining what it
-- does and why it equals what it equals.

-- first let's change the string to a list of individual characters so its
-- easier to work with.

test01 = String.toList "abc"

-- output = test01

-- String.toList creates a list out of all charaters within a string.
-- This produces ["a", "b", "c"]

-- now we make a function that takes an item and puts it in a list of length 2
doubleOneItem : a -> List a
doubleOneItem x = [x, x]

-- ASSIGNMENT: write the answer to these question. what does this following
-- statement produce? is it getting closer to what we want?  why is it not
-- exactly what we want?
test02 = List.map doubleOneItem test01

-- This maps over our list ["a", "b", "c"] and doubles each charactor
-- This results in [["a", "a"], ["b", "b], ["c", "c"]]
-- We have now douplicated each charactor in our list.
-- Now we need to format this as a string


-- output = test02

-- NOW we can use concat to get closer
test03 = List.concat test02
-- This removes inner lists.
-- result: ["a", "a", "b", "b", "c", "c"]

-- output = test03

-- and we still need to produce a string from the result. To convert that list
-- of characters into a string we use String.fromList.
test04 = String.fromList test03

-- output = test04
-- This final function turns list of characters into a single string

-- ASSIGNMENT: convert all of the preceding into a single function
--
--   (you can use the 'let'..'in' form if you need to, or perhaps pipes)
--
--   (even put the definition of 'doubleOneItem' in the new function... maybe
--   use the let..in form or use a lambda)

doubleEachCharacter : String -> String
doubleEachCharacter s =
          s
            |> String.toList
            |> List.map (\x -> [x, x])
            |> List.concat
            |> String.fromList

-- output = doubleEachCharacter "truck"

-- ASSIGNMENT: use the function List.concatMap to do the same thing as
-- List.concat and List.map as you used them above.
--
-- List.concatMap : (a -> List b) -> List a -> List b
-- List.concatMap f input == ...
--
-- What this does is map f over the list input, producing a list of lists and
-- then it concatenates that list, producing a final flat list.
--
-- HOW CAN YOU replace 'map' and 'concat' with this single function?

doubleEachCharacterV2 : String -> String
doubleEachCharacterV2 s =
          s
            |> String.toList
            |> List.concatMap (\x -> [x, x])
            |> String.fromList

-- output = doubleEachCharacterV2 "truck"

-- ASSIGNMENT:
--
-- write something like the previous, but modify doubleOneItem so it doesn't
-- necessarily double every item. write a function that solves the following
-- puzzle:
--
--   take "abc" to "aaabbccc"
--        "xby" to "xxxbbyyy"

doubleOneItemV2 : Char -> List Char
doubleOneItemV2 x =
  case x of
    'b'  -> ['b', 'b']
    _    -> [x, x, x]

doubleEachCharacterV3 : String -> String
doubleEachCharacterV3 s =
          s
            |> String.toList
            |> List.concatMap doubleOneItemV2
            |> String.fromList

output = doubleEachCharacterV3 "abcdef"
