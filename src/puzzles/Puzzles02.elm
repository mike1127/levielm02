
import Html exposing (..)

main =
  output
    |> Debug.toString
    |> text

----------------------------------------------------------------------

-- Write func01.
--
--   Give it [1, -1, 100, -20, -20, 200, 10, 9]
--   Get back [1, 1, -1, -1, 100, -20, -20, 200, 10, 9, 9].
--
--   Give it with [7, 11, -2, 8, 20]
--   Get back [7, 7, 11, -2, -2, 8, 8, 20]


data01 = [1, -1, 100, -20, 200, 10, 9]

func01 : List Int -> List Int
func01 xs =
  let
    d : Int -> List Int
    d x =
      if x < 10
        then [x, x]
        else [x]
  in
    xs
      |> List.concatMap d

-- output = func01 data01

-- hints: write a simpler function that solves part of the problem. Then you
-- can use List.concatMap.


----------------------------------------------------------------------

-- Write func02
--
-- Give it [0, 3, 2, -1, 4, 5]
-- Get back [3, 3, 3, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5]

data02 = [0, 3, 2, -1, 4, 5]

func02 : List Int -> List Int
func02 xs =
  let
    d : Int -> List Int
    d x = List.repeat x x
  in
    List.concatMap d xs

-- output = func02 data02
-- Hint: look at List.repeat.

----------------------------------------------------------------------

-- Write func03
--
-- Give it "awblmx"
-- Get back "aawbbllmx"
--
-- Give it "jklmnop"
-- Get back "jjkkllmnop"
--
-- Give it "abcxyz"
-- get back "aabbccxyz"
--

data03 = "awblmx"

func03 : String -> String
func03 s =
  let
    d : Char -> List Char
    d c =
      if c < 'm'
        then [c, c]
        else [c]
  in
    String.fromList <| List.concatMap d (String.toList s)

-- output = func03 data03

-- Hint. This is similar in some ways to func01, but you're comparing
-- characters, not integers. Try and see if that works or there's something
-- else you need. Don't forget the String functions you'll need to convert a
-- string to a list of characters and back to a String.

----------------------------------------------------------------------

-- New idea: folding.
--
--   PLEASE NOTE: MAKE A PICTURE for every problem here showing the fold.
--
--   For every exercise do the following: (and save it and show it to me on
--   Sunday) --
--
--   1. Take a blank sheet of paper and write the input list at the
--      top.
--
--   2. Write the initial value (on which side of the list? does it depend on
--   whether we're doing a foldl or a foldr?)
--
--   3. Draw lines showing how we're choosing pairs of data to combine.


----------------------------------------------------------------------
--
-- Exercise: write a fold from the left (foldl) that sums all the even numbers
-- in a list, but ignores the odd numbers.
--

data04 = [1, 5, 6, 3, 9, 2, 8]

-- correct output is 16

fold04 : List Int -> Int
fold04 xs =
  let
     f : Int -> Int -> Int
     f value accum =
       if modBy 2 value == 0
         then accum + value
         else accum
  in
    List.foldl f 0 xs

-- output = fold04 data04

----------------------------------------------------------------------
--
--
-- Exercise: take a string, split it into words, and do a fold that
-- reassembles just the words that are longer than 4 characters.
--
-- for instance
--
data05 = "Four score and seven years ago"
output05 = "score seven years"
--
-- You'll have to fiddle with the spacing of the resulting words and there is
-- more than one way to do this fold.
--

func05 : String -> String
func05 s =
  s
    |> String.split " "
    |> List.filter (\i -> String.length i > 4)
    |> List.intersperse " "
    |> String.concat


-- output = func05 data05


-- Part 2: repeat this exercise but use one or more of the following:
-- List.filter, List.concat, String.join, etc. Note that these are all special
-- built-in folds.

----------------------------------------------------------------------
--
-- Exercise: first use List.filter to take a list of ints and produce a new
-- list containing only the negative numbers, preserving their order.
--

data06L = [10, -23, 5, -19, -7, 153, 0]

func06L : List Int -> List Int
func06L xs =
  xs
    |> List.filter (\i -> i < 0)

-- output = func06L data06L

-- Part 2: implement your own version of List.filter using a fold. Definitely
-- draw a diagram of this on paper before trying to code it. Try to do this
-- both as a foldr and a foldl.

data06 = [1, -7, -100, 8, 2000, -4]
output06 = [-7, -100, -4]

d06 : Int -> List Int -> List Int
d06 i xs =
    if i < 0
      then i :: xs
      else xs

func06l : List Int -> List Int
func06l xs =
    List.foldl d06 [] xs

func06r : List Int -> List Int
func06r xs =
    List.foldr d06 [] xs

dataF = ['c', 'h', 'a', 'i', 'r']

onlyVowels : Char -> Bool
onlyVowels c = List.member c ['a', 'e', 'i', 'o', 'u']

ourFilter : (a -> Bool) -> List a -> List a
ourFilter pred xs =
  let
    step : a -> List a -> List a
    step value accum =
      if pred value
        then value :: accum
        else accum
  in
    List.foldr step [] xs

output = ourFilter onlyVowels dataF
