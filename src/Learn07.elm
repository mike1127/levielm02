
import Browser
import ViewData exposing (showMaybe, showList)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


----------------------------------------------------------------------
----------------------------------------------------------------------
--             Levi, ignore this section

type Msg
  = ButtonClick


main =
  Browser.sandbox { init = init, update = update, view = view }


update : Msg -> Model -> Model
update msg model = 
  case msg of
    ButtonClick -> { model | result = operate model.result }


view : Model -> Html Msg
view model =
  div [] 
    [ display model
    , input 
      
        [ type_ "button", value "a button", onClick ButtonClick ] 
      
        []
    ]

----------------------------------------------------------------------
----------------------------------------------------------------------


----------------------------------------------------------------------
-- INTRODUCTION: 
--
-- This is the first time we will introduce mapping a function over a list. 


----------------------------------------------------------------------
-- STEP 1: INTIALIZE some test data in a variable with a name like 
--         testData01, testData02, etc.
----------------------------------------------------------------------

testData01 : List Int
testData01 = [1, 2, 3, 4, 5, 6]


-- FOR TEST DATA IN THIS FILE, we'll use 'vehicle1'

----------------------------------------------------------------------
-- STEP 2: GIVE 'result' a TYPE SIGNATURE the same as your test data.
----------------------------------------------------------------------
type alias Model =
  { result      : List Int
  }


----------------------------------------------------------------------
-- STEP 3: INITIALIZE the 'result' field to the TEST DATA VARIABLE that
--         you have chosen.
----------------------------------------------------------------------
init : Model
init =
  { result = testData01 }


----------------------------------------------------------------------
-- STEP 4: define a function for OPERATING on your test data.
--
-- For this file, we'll change the vehicle to a truck.
--
----------------------------------------------------------------------

someFunction : Int -> Int
someFunction x = x * x

operate : List Int -> List Int
operate l = List.map someFunction l


----------------------------------------------------------------------
-- STEP 5: PUT the correct function at the location below for SHOWING the
--         'result' field (that means CONVERTING to a string).
--
----------------------------------------------------------------------
display : Model -> Html Msg
display model = 
  let 
      --------------------------------------------------
      -- PUT the correct function here 
      --------------------------------------------------
      s = showList String.fromInt model.result
  in 
      div [] [text s]




