
import Browser
import ViewData exposing (showMaybe, showList)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


----------------------------------------------------------------------
----------------------------------------------------------------------
--             Levi, ignore this section

type Msg
  = ButtonClick


main =
  Browser.sandbox { init = init, update = update, view = view }


update : Msg -> Model -> Model
update msg model = 
  case msg of
    ButtonClick -> { model | result = operate model.result }


view : Model -> Html Msg
view model =
  div [] 
    [ display model
    , input 
      
        [ type_ "button", value "a button", onClick ButtonClick ] 
      
        []
    ]


----------------------------------------------------------------------
----------------------------------------------------------------------


testData01 : List Int
testData01 = [1, 2, 3, 4, 5, 6]


addOneToMaybeInt : Maybe Int -> Maybe Int
addOneToMaybeInt m = 
  case m of
    Nothing -> Nothing
    Just x  -> Just <| x + 1


multTwoByMaybeInt : Maybe Int -> Maybe Int
multTwoByMaybeInt m = 
  case m of
    Nothing -> Nothing
    Just x  -> Just <| 2 * x


type alias Model =
  { result      : Maybe Int
  }


init : Model
init =
  { result = Just 5 }


operate : Maybe Int -> Maybe Int
operate = addOneToMaybeInt


display : Model -> Html Msg
display model = 
  let 
      --------------------------------------------------
      -- PUT the correct function here 
      --------------------------------------------------
      s = showMaybe String.fromInt model.result
  in 
      div [] [text s]



