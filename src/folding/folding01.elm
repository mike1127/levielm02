import Html exposing (..)

main = text <| Debug.toString output

list01 : List Int
list01 = [1, 2, 3, 4, 5]

list02 : List Int
list02 = []

list03 : List (List Int)
list03 = [ [4, 5], [7, 0, 1], [3, 9] ]

f : List a -> List a -> List a
f x y = y ++ x

f2 : Int -> Int -> Int
f2 x y = y - x

step : Int -> (Int, Int) -> (Int, Int)
step c (a, b)=
  if modBy 2 c == 0
    then (a + c, b)
    else (a, b + c)

output01 = List.product (list01 ++ list02)

output02 = List.foldr (-) 0 [3, 4, 7, 9]

output = List.foldr step (0, 0) [2, 7, 9, 3, 0]
