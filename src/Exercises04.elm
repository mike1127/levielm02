import Debug
import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)


main =
  Html.text <| Debug.toString output01B


-- begin researching web stuff

----------------------------------------------------------------------
-- for each of the following, write the following three things:
--
--   1. a function that implements the type signature using only operations on
--   generic types
--
--   2. two examples of using that function
--


-- an example to show what I mean:

-- type signature:
test01 : (a -> b) -> (a, a) -> (b, b)

-- implementatation for generic types
test01 f (x, y) = (f x, f y)

-- an example of using 'test01'
output01A = test01 String.toInt ("1234", "abc")

output01B = test01 round (1.1, 3.9)

----------------------------------------------------------------------

test02 : (a -> b) -> (b -> c) -> a -> c
test02 = Debug.todo "TODO"



----------------------------------------------------------------------
-- try to do two different implementations of this

test03 : a -> [a]
test03 = Debug.todo "TODO"


----------------------------------------------------------------------
-- think this one out carefully, step by step. start by asking yourself how
-- many arguments test04 has

test04 : (a -> a) -> a -> a -> a
test04 = Debug.todo "TODO"


----------------------------------------------------------------------


test05 : (a -> b -> c) -> (a, b) -> c
test05 = Debug.todo "TODO"


----------------------------------------------------------------------
-- Exercise 00A

-- keep the tuples that have a True and concatenate their strings.

test00A_1 = [(False, "bob"), (True, "fred"), (True, "peter"), (False, "mary")]
goal00A = "fredpeter"


----------------------------------------------------------------------
-- Exercise 00B

test00B_1 = [1, 2, 3, 4, 5]
test00B_2 = [6, 7, 8, 9, 10]
goal00B = [24, 36, 50]

-- multiply the list elements together with List.map2 and keep the products
-- over 20

----------------------------------------------------------------------

-- Exercise 00C
-- 
-- make a list of numbers from 1 to 7. There's a function in Data.List that
-- will help you with this.

goal00C = [1, 2, 3, 4, 5, 6, 7]


----------------------------------------------------------------------
-- Exercise 00D

-- given a list of numbers, find out which ones divide 24 evenly.
--
-- reminder: that means, for a number n, that 24 % n == 0.

test00D = [2, 3, 4, 5, 6, 7, 8]
goal00D = [2, 3, 4, 6, 8]

----------------------------------------------------------------------
-- Exercise 00E

-- Given an int, find all its divisors
--
f_00E : Int -> List Int
f_00E = Debug.todo "TODO"



