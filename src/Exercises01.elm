
import Browser
import ViewData exposing (showMaybe, showList, quoteString)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)

----------------------------------------------------------------------


----------------------------------------------------------------------
-- helpful function

safeTail : List a -> List a
safeTail l = case List.tail l of
  Nothing -> []
  Just t  -> t


----------------------------------------------------------------------
-- Exercise 01: turn a list into another list.
--
--
--
-- NOTE THE USE of 'showList' below: when the exercise is to make a list, you
-- still have to turn it into a string to make it visible in the web page.

input01 = [-1, 3, 2, -10, 7]

goal01 = [-10, -1, 2, 3, 7]

operate01 : List Int -> String
operate01 l = showList String.fromInt (List.sort l)


----------------------------------------------------------------------
----------------------------------------------------------------------
-- Exercise 02: turn two lists into another list.
--
--
--
-- NOTE THE USE of 'showList' below: when the exercise is to make a list, you
-- still have to turn it into a string to make it visible in the web page.

input02A = [1, 3, 0]

input02B = [6, 7, 8]

goal02 = [1, 3, 0, 6, 7, 8]

operate02 : List Int -> List Int -> String
operate02 l1 l2 = showList String.fromInt (l1 ++ l2)

----------------------------------------------------------------------
----------------------------------------------------------------------
-- Exercise 03: take tail of a list
--
--
--
input03 = [1, 3, 0, 7, 9, 10]

goal03 = [3, 0, 7, 9, 10]

operate03 : List Int -> String
operate03 l = showList String.fromInt <| safeTail l


----------------------------------------------------------------------
----------------------------------------------------------------------
-- Exercise 04:
--

input04 = [1, 2, 3]

goal04 = [1, 2, 3, 3, 2, 1]

operate04 : List Int -> String
operate04 l = showList String.fromInt <| (l ++ (List.reverse l))



----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 05

input05 = [1, 5, 3, 2]

goal05 = [1, 5, 3, 2, 3, 5, 1]

operate05 : List Int -> String
operate05 l = showList String.fromInt <| (l ++ safeTail (List.reverse l))


----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 06

input06 = [3, 6, 2, 1, 8]

goal06 = [1, 4, 9, 36, 64]

operate06 : List Int -> String
operate06 l = showList String.fromInt <| List.map (\x -> x*x) <| List.sort l


----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 07

-- this shows how to turn a list of strings into a single string
-- quoteString isnt defined in viewData.

input07 = ["aba", "cdc", "efe", "gth"]

operate07 : List String -> String
operate07 l = showList quoteString l


----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 08


input08 = ["cat", "dog", "fish", "zebra"]

-- the idea here is to keep any strings with length > 3
goal08 = ["fish", "zebra"]

operate08 : List String -> String
operate08 l = showList quoteString <| List.filter (\x -> String.length x > 3) l



-- FOR TEST DATA IN THIS FILE, we'll use 'vehicle1'

----------------------------------------------------------------------
-- STEP 2: GIVE 'result' a TYPE SIGNATURE the same as your test data.
----------------------------------------------------------------------
type alias Model =
  { result      : String
  }


----------------------------------------------------------------------
-- STEP 3: INITIALIZE the 'result' field to the TEST DATA VARIABLE that
--         you have chosen.
----------------------------------------------------------------------
init : Model
init =
  { result = operate08 input08 }


----------------------------------------------------------------------
-- STEP 4: define a function for OPERATING on your test data.
--
-- For this file, we'll change the vehicle to a truck.
--
----------------------------------------------------------------------

someFunction : Int -> Int
someFunction x = x * x

operate : String -> String
operate l = l


----------------------------------------------------------------------
-- STEP 5: PUT the correct function at the location below for SHOWING the
--         'result' field (that means CONVERTING to a string).
--
----------------------------------------------------------------------
display : Model -> Html Msg
display model =
  let
      --------------------------------------------------
      -- PUT the correct function here
      --------------------------------------------------
      s = model.result
  in
      div [] [text s]




----------------------------------------------------------------------
----------------------------------------------------------------------
--             ignore this section

type Msg
  = ButtonClick


main =
  Browser.sandbox { init = init, update = update, view = view }


update : Msg -> Model -> Model
update msg model =
  case msg of
    ButtonClick -> { model | result = operate model.result }


view : Model -> Html Msg
view model =
  div []
    [ display model
    , input

        [ type_ "button", value "a button", onClick ButtonClick ]

        []
    ]

----------------------------------------------------------------------
----------------------------------------------------------------------
