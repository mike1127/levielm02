import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Random


main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = \_ -> Sub.none
    , view = view
    }

type Msg = ChangeInput String | Submit

type GameState = JustStarted | HighOrLow | Error | Won

type alias Model =
  { gameState : GameState
  , lastInput : String
  , lastSubmission : String
  , lastSubmittedNumber : Int
  , theNumber : Int}

init : () -> (Model, Cmd Msg)
init _ =
  ( { gameState = JustStarted
    , lastInput = ""
    , lastSubmission = ""
    , theNumber = 100 }, Cmd.none )

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
      ChangeInput s -> updateChangeInput s model
      Submit        -> updateSubmit model

updateChangeInput : String -> Model -> (Model, Cmd Msg)
updateChangeInput s model = ( { model | lastInput = s }, Cmd.none)

updateSubmit : Model -> (Model, Cmd Msg)
updateSubmit model = ( { model | lastSubmission = model.lastInput}, Cmd.none)

view : Model -> Html Msg
view m =
  case m.gameState of
    JustStarted   ->
      let
        elem = (p [] [text "Go ahead, enter a number... "])
      in
        viewCombined m elem
    HighOrLow   ->
      let
        elem = if 150 > m.theNumber
                then p [] [text "A little lower"]
                else p [] [text "A little higher"]
      in
        viewCombined m elem
    Error   ->
        let
          elem =
            (p [] [text ("Error: " ++ m.lastSubmission ++ " is not a number")])
        in
          viewCombined m elem
    Won         -> viewWon

viewJustStarted : Model -> Html Msg
viewJustStarted m =
    div []
      [ p [] [text (Debug.toString m)]
      , span [] [text "Enter a number: " ]
      , input [ type_ "text", placeholder "Type something here"
              , value m.lastInput, onInput ChangeInput ] []
      , button [onClick Submit] [text "Submit"]
      ]
{-
viewHighOrLow : Model -> String -> Html Msg
viewHighOrLow m higherLower =
    div []
      [ p [] [text (Debug.toString m)]
      , p [] [text <| "A little " ++ higherLower ++ "..."]
      , span [] [text "Enter a number: " ]
      , input [ type_ "text", placeholder "Type something here"
              , value m.lastInput, onInput ChangeInput ] []
      , button [onClick Submit] [text "Submit"]
      ]
-}

viewCombined : Model -> Html Msg -> Html Msg
viewCombined m elem=
  div []
    [ p [] [text (Debug.toString m)]
    , elem
    , span [] [text "Enter a number: " ]
    , input [ type_ "text", placeholder "Type something here"
            , value m.lastInput, onInput ChangeInput ] []
    , button [onClick Submit] [text "Submit"]
    ]


viewWon =
    div []
      [ p [] [text <| "You won!"] ]
