import Random
import Random.String exposing (..)
import Random.Char exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Browser

type Msg = ChangeInput String | Submit | NewString String

type alias Model = { lastInput  : String
                   , lastSubmit : String
                   , theString  : String }

main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = \_ -> Sub.none
    , view = view }

init : () -> (Model, Cmd Msg)
init _ =
  ({ lastInput = ""
   , lastSubmit = ""
   , theString = "bca"},
   Random.generate NewString (Random.String.string 5 Random.Char.english))

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    ChangeInput s ->
      ({ model | lastInput = s }, Cmd.none)
    Submit        ->
      ({ model | lastSubmit = model.lastInput }, Cmd.none)
    NewString s   ->
      ( { model | theString = String.toLower s }, Cmd.none)

view : Model -> Html Msg
view model =
    let
      elem = if model.lastSubmit == ""
                then "Go ahead, enter something..."
                else if model.lastSubmit /= sortAlphabetically model.theString
                  then "Keep trying..."
                  else "Correct!"
    in
      div []
      [
        p [] [text ("Enter '" ++ model.theString ++ "' in alphabetical order.")]
      , p [] [text elem]
      , input [ type_ "text"
              , placeholder "Enter something here"
              , value model.lastInput
              , onInput ChangeInput] []
      , button [onClick Submit] [text "Click me!"]
      ]

sortAlphabetically : String -> String
sortAlphabetically s =
  s
    |> String.toList
    |> List.sort
    |> String.fromList
