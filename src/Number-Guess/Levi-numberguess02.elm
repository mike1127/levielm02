import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Random exposing (..)


main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = \_ -> Sub.none
    , view = view
    }

type Msg = ChangeInput String | Submit | NewNumber Int


type alias Model =
  { lastInput : String
  , lastSubmission : String
  , theNumber : Int}

init : () -> (Model, Cmd Msg)
init _ =
  ( { lastInput = ""
    , lastSubmission = ""
    , theNumber = 100 }, Random.generate NewNumber (Random.int 1 10) )

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
      ChangeInput s -> updateChangeInput s model
      Submit        -> updateSubmit model
      NewNumber i   -> ({ model | theNumber = i }, Cmd.none)

updateChangeInput : String -> Model -> (Model, Cmd Msg)
updateChangeInput s model = ( { model | lastInput = s }, Cmd.none)

updateSubmit : Model -> (Model, Cmd Msg)
updateSubmit model = ( { model | lastSubmission = model.lastInput}, Cmd.none)

view : Model -> Html Msg
view model =
  case String.toInt model.lastSubmission of
    Just i ->
      let
        elem = if i > model.theNumber
                then "A little lower... "
                else if i < model.theNumber
                  then "A little higher... "
                  else "Correct! "
      in
        view2 elem model
    Nothing ->
      let
        s = if model.lastSubmission == ""
              then "Please enter a number"
              else "Error: '" ++ model.lastSubmission ++ "' is not a number."
      in
        view2 s model


viewActualNumber : Int -> Model -> Html Msg
viewActualNumber i model =
  let
    elem = if i > model.theNumber
            then p [] [text "A little lower... "]
            else if i < model.theNumber
              then p [] [text "A little higher... "]
              else p [] [text "Correct! "]
  in
    div []
      [ p [] [text (Debug.toString model)]
      , elem
      , span [] [text "Enter a number: " ]
      , input [ type_ "text", placeholder "Type something here"
              , value model.lastInput, onInput ChangeInput ] []
      , button [onClick Submit] [text "Submit"]
      ]

viewError : Model -> Html Msg
viewError model =
  div []
    [ p [] [text (Debug.toString model)]
    , p [] [text <| "Please enter a number. "]
    , span [] [text "Enter a number: " ]
    , input [ type_ "text", placeholder "Type something here"
            , value model.lastInput, onInput ChangeInput ] []
    , button [onClick Submit] [text "Submit"]
    ]

view2 : String -> Model -> Html Msg
view2 s model =
      div []
        [ p [] [text (Debug.toString model)]
        , p [] [text s]
        , span [] [text "Enter a number: " ]
        , input [ type_ "text", placeholder "Type something here"
                , value model.lastInput, onInput ChangeInput ] []
        , button [onClick Submit] [text "Submit"]
        ]
