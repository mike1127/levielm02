module ViewData exposing (..)

import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


showTuple : (a -> String) -> (b -> String) -> (a, b) -> String
showTuple f1 f2 (t1, t2) = "(" ++ f1 t1 ++ ", " ++ f2 t2 ++ ")"


quoteString : String -> String
quoteString s = "'" ++ s ++ "'"


numToDiv : (a -> String) -> a -> Html b
numToDiv f n =
  div [] [ text <| f n ]


numToSpan : (a -> String) -> a -> Html b
numToSpan f n =
  span 

    [ style "margin-left" "5px"
    , style "margin-right" "5px"
    ] 

    [ text <| f n ]


listToHtml : (a -> Html b) -> List a -> Html b
listToHtml f l = 
  div [] (List.map f l)


showList : (a -> String) -> List a -> String
showList f l =
  let
      s = String.concat <| List.intersperse ", " <| List.map f l
  in
      "[ " ++ s ++ " ]"


showMaybe : (a -> String) -> Maybe a -> String
showMaybe f x = case x of
   Nothing -> "Nothing"
   Just y  -> "Just ( " ++ f y ++ " )"

