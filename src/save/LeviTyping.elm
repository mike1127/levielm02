
-- Exercises in function composition

import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)

main =
  Html.text <| Debug.toString output

----------------------------------------------------------------------
--

type Vehicle = Car String (List String)
             | Truck String (List String)

testVehicle1 = Car   "Honda"   ["George", "Fred"]
testVehicle2 = Truck "Toyoda"  ["Mary"]
testVehicle3 = Truck "Navigator" ["kid 1", "kid 2", "kid 3"]
testVehicle4 = Car   "Google"    []

testVehicleList = [testVehicle1, testVehicle2, testVehicle3, testVehicle4]

getOccupants : Vehicle -> List String
getOccupants v =
  case v of
    Car _ occupants   -> occupants
    Truck _ occupants -> occupants

getMake : Vehicle -> String
getMake v =
  case v of
    Car   make _ -> make
    Truck make _ -> make

isInVehicle : String -> Vehicle -> Bool
isInVehicle s v =
  v
    |> getOccupants
    |> List.member s

allOccupantsofList : List Vehicle -> List String
allOccupantsofList = List.map getOccupants >> List.concat

-- any : (a -> Bool) -> List a -> Bool

maryIsInAnyVehicle : List Vehicle -> Bool
maryIsInAnyVehicle vs = List.any (isInVehicle "") vs

-- output = maryIsInAnyVehicle testVehicleList

----------------------------------------------------
--      Address

type alias Name = String

type alias Street = String

type Address = StreetAddress Name Int Street
             | POBoxAddress  Name Int

getAddressName addr =
  case addr of
    StreetAddress name _ _ -> name
    POBoxAddress  name _   -> name

getHouseNumber : Address -> Maybe Int
getHouseNumber a =
  case a of
    StreetAddress _ number _ -> Just number
    POBoxAddress  _ _        -> Nothing

anyHouseLessThan500 = List.filterMap getHouseNumber >> List.any ((>) 50)



checkGreaterThan500 : Int -> Bool
checkGreaterThan500 = (>) 50
-- filterMap : (a -> Maybe b ) -> List a -> List b

testAddress1 = StreetAddress "Bob" 999 "Excelsior"
testAddress2 = POBoxAddress "Jonny" 1

testNumber1 = 300
testNumber2 = 600

testAddresses : List Address
testAddresses = [ StreetAddress "Paul" 460 "Oakland"
                , POBoxAddress "Mike" 94611
                , POBoxAddress "Brenda" 12345
                , StreetAddress "Fred" 111 "Jewel" ]


-- output = List.filterMap getHouseNumber testAddresses
output = anyHouseLessThan500 testAddresses
