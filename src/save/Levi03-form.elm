import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)



-- MAIN

-- there is always a main which is apparently a type called sandbox in browser
--
main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
  { name : String
  , year : String
  , password : String
  , passwordAgain : String
  }


init : Model
init =
  Model "" "" "" ""



-- UPDATE


type Msg
  = Name String
  | Year String
  | Password String
  | PasswordAgain String


update : Msg -> Model -> Model
update msg model =
  case msg of
    Name name ->
      { model | name = name }

    Year year ->
      { model | year = year }

    Password password ->
      { model | password = password }

    PasswordAgain password ->
      { model | passwordAgain = password }



-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ viewInput "text" "Name" model.name Name
    , viewInput "year" "Year of Birth" model.year Year
    , viewInput "password" "Password" model.password Password
    , viewInput "password" "Re-enter Password" model.passwordAgain PasswordAgain
    , viewValidation model
    , viewYearCheck model
    ]

-- type_, placeholder, value, onInput


-- input: onInput call toMsg
viewInput : String -> String -> String -> (String -> msg) -> Html msg
viewInput t p v toMsg =
  input [ type_ t, placeholder p, value v, onInput toMsg ] []

ownStringToInt s =
  let
    maybeInt = String.toInt s
  in
    Maybe.withDefault 0 maybeInt

-- viewValidation : Checks the criteria of Model, and compairs them to
viewValidation : Model -> Html msg
viewValidation model =
  if String.length model.password < 8 then
    div [ style "color" "red" ] [ text "Password not long enough" ]
  else

    if model.password == model.passwordAgain then
      div [ style "color" "green" ] [ text "OK" ]
    else
      div [ style "color" "red" ] [ text "Passwords do not match!" ]

viewYearCheck : Model -> Html msg
viewYearCheck model =
    if ownStringToInt model.year < 2010 then
      div [ style "color" "green" ] [ text "Old enough" ]
    else
      div [ style "color" "red" ] [ text "Not old enough" ]

{-    div [ style "color" "green" ] [ text "OK" ] -}
