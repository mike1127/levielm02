import Debug
import Browser
import Html exposing (Html, Attribute, div, input, text, span, button)

main =
  Html.text <| Debug.toString output02A


-- begin researching web stuff

----------------------------------------------------------------------
-- for each of the following, write the following three things:
--
--   1. a function that implements the type signature using only operations on
--   generic types
--
--   2. two examples of using that function
--


-- an example to show what I mean:

-- type signature:
test01 : (a -> b) -> (a, a) -> (b, b)

-- implementatation for generic types
test01 f (x, y) = (f x, f y)

-- an example of using 'test01'
-- output01A = test01 String.toInt ("1234", "abc")

-- output01B = test01 round (1.1, 3.9)

----------------------------------------------------------------------

-- fromInt : Int -> String
-- toInt : String -> Maybe Int

test02 : (a -> b) -> (b -> c) -> a -> c
test02 f g x = g <| f <| x

test02v2 : String -> String
test02v2 x = String.fromInt <| String.toInt x


output02A = test02v2 "99"

addB : Int -> Int
addB i = i + 5

myMultiply : Int -> Int
myMultiply i = 2 * i

output02B = test02 addB myMultiply 2


----------------------------------------------------------------------
-- try to do two different implementations of this

test03 : a -> List a
test03 x = [x]

output03A = test03 (3, "Three")

output03B = test03 [1, 2]


----------------------------------------------------------------------
-- think this one out carefully, step by step. start by asking yourself how
-- many arguments test04 has

test04 : (a -> a) -> a -> a -> a
test04 = Debug.todo "TODO"


----------------------------------------------------------------------


test05 : (a -> b -> c) -> (a, b) -> c
test05 = Debug.todo "TODO"


----------------------------------------------------------------------
-- Exercise 00A

-- keep the tuples that have a True and concatenate their strings.

test00A_1 = [(False, "bob"), (True, "fred"), (True, "peter"), (False, "mary")]
goal00A = "fredpeter"

output00A = String.concat <| List.map modTuple test00A_1

modTuple : (Bool, String) -> String
modTuple (a, b) =
  case a of
    True -> b
    False -> ""




----------------------------------------------------------------------
-- Exercise 00B

test00B_1 = [1, 2, 3, 4, 5]
test00B_2 = [6, 7, 8, 9, 10]
goal00B = [24, 36, 50]

-- multiply the list elements together with List.map2 and keep the products
-- over 20

output00B = List.filter (\x -> x > 20) (List.map2 (\x y-> x * y) test00B_1 test00B_2)

----------------------------------------------------------------------

-- Exercise 00C
--
-- make a list of numbers from 1 to 7. There's a function in Data.List that
-- will help you with this.

-- Could not find Data.list

goal00C = [1, 2, 3, 4, 5, 6, 7]

output00C = List.range 1 7
----------------------------------------------------------------------
-- Exercise 00D

-- given a list of numbers, find out which ones divide 24 evenly.
--
-- reminder: that means, for a number n, that 24 % n == 0.

test00D = [2, 3, 4, 5, 6, 7, 8]
goal00D = [2, 3, 4, 6, 8]

output00D = List.filter (\x -> modBy x 24 == 0) test00D



----------------------------------------------------------------------
-- Exercise 00E

-- Given an int, find all its divisors
--
f_00E : Int -> List Int
f_00E i = List.filter (\x -> modBy x i == 0) (List.range 1 i)

output00E = f_00E 100
