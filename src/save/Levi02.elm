
import Browser
import ViewData exposing (showMaybe, showList, quoteString)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Debug
--------------------------------------------------------------------


----------------------------------------------------------------------
-- helpful function



safeTail : List a -> List a
safeTail l = case List.tail l of
  Nothing -> []
  Just t  -> t


----------------------------------------------------------------------
-- Exercise 01: turn a list into another list.
--
--
--
-- NOTE THE USE of 'showList' below: when the exercise is to make a list, you
-- still have to turn it into a string to make it visible in the web page.

input01 = "fred"

goal01 = "fredfrederferf"

-- fred fred tail reverse fred tail reverse fred

-- modelInitialValue =
--   let
--     a1 = input01
--     a2 = String.dropLeft 1 <| String.reverse input01
--   in
--     a1 ++ a1 ++ a2 ++ a2
----------------------------------------------------------------------
----------------------------------------------------------------------
-- Exercise 02: turn two lists into another list.
--
--
--
-- NOTE THE USE of 'showList' below: when the exercise is to make a list, you
-- still have to turn it into a string to make it visible in the web page.

input02A = [1, 3, 0, 2, 5]

input02B = [4, 6, 1, 3, 0]

goal02 = [4, 18, 0, 6, 0]

-- modelInitialValue = showList String.fromInt <| List.map2 (*) input02A input02B

-- myMultiply : Int -> Int -> Int
-- myMultiply x y = x * y

----------------------------------------------------------------------
----------------------------------------------------------------------
-- Exercise 03
--
--
--
input03A = [1, 3, 0, 7, 9, 10]
input03B = [0, 2, 8, 5, 7, 12]

goal03  = [1, 1, -8, 2, 2, -2]

-- modelInitialValue = showList String.fromInt <| List.map2 (-) input03A input03B
----------------------------------------------------------------------
----------------------------------------------------------------------
-- Exercise 04:
--

input04 = (2, "Bob")

goal04 = "BobBob"

-- repeatString : (Int, String) -> String
-- repeatString = crash "foo"

-- modelInitialValue = help input04
-- modelInitialValue =
--   let
--     (a, b) = input04
--   in
--     String.repeat a b

help : (Int, String) -> String
help (x, y) = String.repeat x y

flip : (a, b) -> (b, a)
flip (x, y) = (y, x)

flip2 : (a, b) -> ((a, a), (b, b))
flip2 (x, y) = ((x, x), (y, y))

flip3 : (a, b) -> List a
flip3 (x, _) = [x]
----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 05

input05A = [1, 2, 3, 2]
input05B = ["Fred", "Mary", "Joe", "Frank"]
goal05 = [(1, "Fred"), (2, "Mary"), (3, "Joe"), (2, "Frank")]

-- modelInitialValue = Debug.toString <| List.map2 Tuple.pair input05A input05B

-- t : a -> b -> (a, b)
----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 06

input06A = [1, 2, 3, 2]
input06B = ["Fred", "Mary", "Joe", "Frank"]

goal06 = "FredMaryMaryJoeJoeJoeFrankFrank"

-- modelInitialValue = Debug.toString <| String.concat <| List.map2 (\x y -> String.repeat x y) input06A input06B

-- l : Int -> String -> String
-- l x s = String.repeat x s
--
-- m2 : (Int -> String -> String) -> List Int -> List String -> List String
--
-- m2 : (a -> b -> c) -> List a -> List b -> List c

----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 07

-- this shows how to turn a list of strings into a single string

input07 = ["aba", "cdc", "efef", "gthcs"]

-- test = ["aa", "dd"]
goal07 = ["xxx", "xxx", "xxx", "xxxxx"]

replaceWithChar : String -> String -> String
replaceWithChar replacementChar originalString =
  String.repeat (String.length originalString) replacementChar


replaceWithX : String -> String
replaceWithX =
  replaceWithChar "x"

-- modelInitialValue = showList quoteString <| List.map2 replaceWithChar input07 ["x"]
modelInitialValue = Debug.toString <| testFunction input07

testFunction : List String -> List String
testFunction ls = List.map (replaceWithChar "123") ls


makeNChar ch n = String.concat <| List.repeat n ch


----------------------------------------------------------------------
----------------------------------------------------------------------
--   Exercise 08


input08 = ["cat", "dog", "fish", "zebra"]

-- the idea here is to keep any strings with length > 3
goal08 = ["fish", "zebra"]


-- modelInitialValue = showList quoteString <| List.filter (\x -> String.length x > 3) input08


-- FOR TEST DATA IN THIS FILE, we'll use 'vehicle1'

----------------------------------------------------------------------
-- STEP 2: GIVE 'result' a TYPE SIGNATURE the same as your test data.
----------------------------------------------------------------------
type alias Model =
  { result      : String
  }


----------------------------------------------------------------------
-- STEP 3: INITIALIZE the 'result' field to the TEST DATA VARIABLE that
--         you have chosen.
----------------------------------------------------------------------
init : Model
init =
  { result = modelInitialValue }


----------------------------------------------------------------------
-- STEP 4: define a function for OPERATING on your test data.
--
-- For this file, we'll change the vehicle to a truck.
--
----------------------------------------------------------------------

someFunction : Int -> Int
someFunction x = x * x

operate : String -> String
operate l = l


----------------------------------------------------------------------
-- STEP 5: PUT the correct function at the location below for SHOWING the
--         'result' field (that means CONVERTING to a string).
--
----------------------------------------------------------------------
display : Model -> Html Msg
display model =
  let
      --------------------------------------------------
      -- PUT the correct function here
      --------------------------------------------------
      s = model.result
  in
      div [] [text s]




----------------------------------------------------------------------
----------------------------------------------------------------------
--             ignore this section

type Msg
  = ButtonClick


main =
  Browser.sandbox { init = init, update = update, view = view }


update : Msg -> Model -> Model
update msg model =
  case msg of
    ButtonClick -> { model | result = operate model.result }


view : Model -> Html Msg
view model =
  div []
    [ display model
    , input

        [ type_ "button", value "a button", onClick ButtonClick ]

        []
    ]

----------------------------------------------------------------------
----------------------------------------------------------------------
