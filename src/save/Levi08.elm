

-- Exercises in function composition

import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)


main =
  Html.text <| Debug.toString output

----------------------------------------------------------------------
----------------------------------------------------------------------
-- TYPE to use

-- (1) Here's a data type with three cases. It describes an appliance

type alias Price = Float

type alias LowTemperature = Float

type alias HighTemperature = Float

type alias FoodName = String

type alias DishName = String

type alias AmountSoap = Float

type Appliance = Fridge Price LowTemperature (List FoodName)
               | Oven Price HighTemperature FoodName
               | Dishwasher Price AmountSoap (List DishName)

----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: describe this data type in English. Write a description of each
-- case, describe the data that goes with that case, and describe why the data
-- is appropriate for that case.

-- ... write here ...

-- Appliance holds three sub types: Fridge, Oven, Dishwasher.

-- Appliance has three constructors, therefore there are three ways to
-- construct an appliance.

-- A fridge holds a price (float) (how much it costs to buy), a low tempurature
-- (float) (the lowest tempurature of the fridge), and a list of foods inside the
-- fridge called (List FoodName) (List Strings).

-- An oven fridge holds a price (float) (how much it costs to buy), a high tempurature
-- (float) (the highest tempurature the oven gets), and a foodName (String), the
-- food that is inside the oven.

-- A dishwasher holds a price (float) (how much it costs to buy), an amount of soap
-- (float) (the amount of soap in the dishwasher), and a list of dishes that are
-- getting washed (List DishName) (List String)


----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: create test data. Make some individual appliances, then put
-- them into a list as well.
--
--

testData01 = Fridge 59.99 38.5 ["Soda", "Butter", "Milk"]
testData02 = Oven 179.99 385.0 "Pizza"
testData03 = Dishwasher 112.59 10 ["Knife", "Mug", "Spoon"]

testList01 = [testData01, testData02, testData03]
----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: write a function of type "Appliance -> Maybe a" that picks a
-- data field out of the type for some cases but not others.

whatsInOven : Appliance -> Maybe FoodName
whatsInOven ap =
  case ap of
    Oven _ _ foodName -> Just foodName
    _                 -> Nothing


-- this is an example for a List of food names. You pick some other field.
getFoodList : Appliance -> Maybe (List FoodName)
getFoodList appl =
  case appl of
      Fridge _ _ foodList     -> Just foodList
      Dishwasher _ _ _        -> Nothing
      Oven _ _ _              -> Nothing


-- output = whatsInOven testData02

----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: write function that can be used for filter.

-- (1) write a function that gets the price

getPrice : Appliance -> Price
getPrice ap =
  case ap of
    Fridge     price _ _  -> price
    Dishwasher price _ _  -> price
    Oven       price _ _  -> price

-- output = getPrice testData03

-- (2) write a function that checks if the price is above or below a certain
-- cutoff

isAcceptablePrice : Float -> Appliance-> Bool
isAcceptablePrice fl ap =
  ap
    |> getPrice
    |> (\n -> n > fl)

-- output = isAcceptablePrice testData02 100

-- (3) write an expression that gets all the appliances in the test data list
-- that are picked by the filter function

isAcceptablePrice100 : Appliance -> Bool
isAcceptablePrice100 = isAcceptablePrice 100

expression01 a =
  a
    |> List.filter (isAcceptablePrice 100)
    |> List.map getPrice

-- output = expression01 testList01

----------------------------------------------------------------------
----------------------------------------------------------------------
-- ASSIGNMENT: write two more filter functions. Start by picking some field,
-- pick some other variable like the number of food items and write filter
-- functions.

getTemperature : Appliance -> Maybe Float
getTemperature a =
  case a of
    Fridge _ temp _ -> Just temp
    Oven   _ temp _ -> Just temp
    _               -> Nothing

mystery : Maybe Float -> Bool
mystery ml =
  case ml of
    Just x -> x < 100
    Nothing -> False

filterTemperatures : List Appliance -> List Appliance
filterTemperatures = List.filter (mystery << getTemperature)


-- tempCutoff100 : Appliance -> Bool
-- tempCutoff100 a =
--     a
--       |> getTemperature
--       |> (\n -> n < 100.0)

-- expression02 a =
--   a
--     |> List.filter tempCutoff100
--     |> List.map getTemperature

filterAppliancesWithTemperature : List Appliance -> List Float
filterAppliancesWithTemperature =
  List.filterMap getTemperature


output = filterAppliancesWithTemperature testList01
