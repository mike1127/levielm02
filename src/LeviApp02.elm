
-- what should I have as an app? sorting, append string to reversed version
--
--


import Browser
import ViewData exposing (showMaybe, showList)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


----------------------------------------------------------------------


testData : List Int
testData = [1, 2, 10, -5, 6, 12]

fibonacci : Int -> Int
fibonacci x =
  case x of
    0 -> 1
    1 -> 1
    _ -> fibonacci(x-1) + fibonacci(x-2)

myToInt : String -> Int
myToInt s = case String.toInt s of
  Just n  -> n
  Nothing -> -100


operate : String -> String
operate x = String.fromInt (fibonacci (myToInt x))


check : Int -> Bool
check i =
  if i <= 6
    then True
    else False

----------------------------------------------------------------------


type Msg
  = Change1 String


type alias Model =
  { field1      : String
  , result      : String
  }

main =
  Browser.sandbox { init = init, update = update, view = view }


init : Model
init =
  { field1 = ""
  , result = "" }


update : Msg -> Model -> Model
update msg model =
  let
    model2 = case msg of
      Change1 s -> { model | field1 = s }
  in
    { model2 | result = operate model2.field1}


view : Model -> Html Msg
view model =
  div []
    [ input [ value model.field1, onInput Change1 ] []
    , div [] [ text model.result ]
    ]
