-- Assignment 1:
--
-- 1. Make sure you understand each part of this file and what it does. Write
-- a few sentences about the following:
--
--   a. What appears to be what this program does? (run it)
--   b. What is the Model defined this way? Why not have more fields?
--   c. Why is 'init' defined that way?
--   d. Why is update defined with only one case?
--   e. what is the structure of the web page created by 'view'
--
-- 2. Notice the 'onInput' attribute. Review the documentation about what this
-- does. (Search for "elm-lang html", find Html.Events and look up onInput.)
-- How many arguments does it take? What are they? 
--
-- 3. Then look at 'sendChangeMsg' defined below. Can you see how this is a
-- function of the right type to provide as the first argument to 'onInput'?
--
-- 4. Now replace 'sendChangeMsg' with something more compact, coded into the
-- input function itself. In the next exercise you'll see how I did it.
--
-- 5. Now, save this file by some other name (include 'Levi' in the name), and
-- modify it as follows.
--
--   - Have two input fields instead of one.
--
--   Think about what needs to change. Does the definition of Msg need to
--   change? Does the model need to change? how does the view change, and how
--   do you fill in the text of the view from the model?
--
-- My own answer is in the next file, several-fields-02.elm.

import Browser
import Html exposing (Html, text, div, input)
import Html.Attributes exposing (type_, value)
import Html.Events exposing (onInput, onClick)


main =
  Browser.sandbox { init = init, update = update, view = view }


type alias Model =
  { field      : String
  }


type Msg = Change String


init : Model
init = { field = "Craig" }


update : Msg -> Model -> Model
update msg model =
  case msg of
      Change s -> { model | field = s }


sendChangeMsg : String -> Msg
sendChangeMsg s = Change s


view : Model -> Html Msg
view model =
  div []
    [ input [ type_ "text", value model.field, onInput sendChangeMsg ]
        []
    ]
