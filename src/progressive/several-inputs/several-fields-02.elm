-- Assignment 2:
--
-- 1. Make sure you understand each part of this file and what it does. Write
-- a few sentences about the following:
--
--   a. What appears to be what this program does? (run it)
--   aa. What are any new elements compared to the last exercise?
--   b. What is the Model defined this way? Why have these fields?
--   c. Why is 'init' defined that way?
--   d. Why is update defined this way?
--   e. what is the structure of the web page created by 'view'?
--
-- 2. Notice the 'onInput' attribute. Review the documentation about what this
-- does. (Search for "elm-lang html", find Html.Events and look up onInput.)
-- How many arguments does it take? What are they? 
--
-- 5. Now, save this file by some other name (include 'Levi03' in the name),
-- and modify it as follows.
--
--   - Have six input fields instead of one. Note that this is starting to get
--     out of hand if we were to have six constructors?
--
--  Try to do this by defining only
--   - one constructor of the Msg: Change Int String.
--
--   type Msg = Change Int String
--
--   How can this constructor provide messages for all six fields? What is the
--   Int doing there? Please write your answer.
--
--  My own answer is in the next file, several-fields-03.elm.

import Browser
import Html exposing (Html, text, div, input)
import Html.Attributes exposing (type_, value)
import Html.Events exposing (onInput, onClick)


main =
  Browser.sandbox { init = init, update = update, view = view }


type alias Model =
  { field1      : String
  , field2      : String
  }


type Msg = Change1 String
         | Change2 String


init : Model
init = { field1 = "Craig"
       , field2 = "Joe" }


update : Msg -> Model -> Model
update msg model =
  case msg of
      Change1 s -> { model | field1 = s }
      Change2 s -> { model | field2 = s }


view : Model -> Html Msg
view model =
  div []
    [ input [ type_ "text", value model.field1, onInput Change1 ]
        []
    , input [ type_ "text", value model.field2, onInput Change2 ]
        []
    ]
