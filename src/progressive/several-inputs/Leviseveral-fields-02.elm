-- Assignment 2:
--
-- 1. Make sure you understand each part of this file and what it does. Write
-- a few sentences about the following:
--
--   a. What appears to be what this program does? (run it)
--   aa. What are any new elements compared to the last exercise?
--   b. What is the Model defined this way? Why have these fields?
--   c. Why is 'init' defined that way?
--   d. Why is update defined this way?
--   e. what is the structure of the web page created by 'view'?
--
-- 2. Notice the 'onInput' attribute. Review the documentation about what this
-- does. (Search for "elm-lang html", find Html.Events and look up onInput.)
-- How many arguments does it take? What are they?
--
-- 5. Now, save this file by some other name (include 'Levi03' in the name),
-- and modify it as follows.
--
--   - Have six input fields instead of one. Note that this is starting to get
--     out of hand if we were to have six constructors?
--
--  Try to do this by defining only
--   - one constructor of the Msg: Change Int String.
--
--   type Msg = Change Int String
--
--   How can this constructor provide messages for all six fields? What is the
--   Int doing there? Please write your answer.
--
--  My own answer is in the next file, several-fields-03.elm.

import Browser
import Html exposing (Html, text, div, input)
import Html.Attributes exposing (type_, value)
import Html.Events exposing (onInput, onClick)


main =
  Browser.sandbox { init = init, update = update, view = view }


type alias Model =
  { field1      : String
  , field2      : String
  , field3      : String
  , field4      : String
  , field5      : String
  , field6      : String
  }


type Msg = Change Int String


init : Model
init = { field1 = "Craig"
       , field2 = "Joe"
       , field3 = "Kieth"
       , field4 = "James"
       , field5 = "Mary"
       , field6 = "Kurt"}


update : Msg -> Model -> Model
update msg model =
  case msg of
      Change 1 s -> { model | field1 = s }
      Change 2 s -> { model | field2 = s }
      Change 3 s -> { model | field3 = s }
      Change 4 s -> { model | field4 = s }
      Change 5 s -> { model | field5 = s }
      Change _ s -> { model | field6 = s }

sendMessage : Int -> String -> Msg
sendMessage i s = Change i s



test = sendMessage 1

view : Model -> Html Msg
view model =
  div []
    [ createDiv model.field1 (Change 1)
    , createDiv model.field2 (Change 2)
    , createDiv model.field3 (Change 3)
    , createDiv model.field4 (Change 4)
    , createDiv model.field5 (Change 5)
    , createDiv model.field6 (Change 6)
    ]

createDiv : String -> (String -> Msg) -> Html Msg
createDiv mo func =
  input [ type_ "text", value mo, onInput func] []
