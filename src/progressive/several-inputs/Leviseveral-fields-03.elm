-- Assignment 3:
--
-- 1. Make sure you understand each part of this file and what it does. Write
-- a few sentences about the following:
--
--   a. What appears to be what this program does? (run it)
--   aa. What are any new elements compared to the last exercise?
--   b. What is the Model defined this way? Why have these fields?
--   c. Why is 'init' defined that way?
--   d. Why is update defined this way?
--   e. what is the structure of the web page created by 'view'?
--
--   SEE OTHER QUESTION BELOW IN THE TEXT: please write answers.
--
-- 5. Now, save this file by some other name (include 'Levi04' in the name),
--    and modify it as follows.
--
--    Define the model as
--
--      type Model = { fields : List String }
--
--
--
--  Try to do this by defining only
--   - one constructor of the Msg: Change Int String.
--
--   type Msg = Change Int String
--
--   How can this constructor provide messages for all six fields? What is the
--   Int doing there? Please write your answer.
--
-- 6. How does this change the view? You have to write a view that displays a
--    list of fields. You will create a function as follows:
--
--    makeDiv : (Int,String) -> Html Msg
--
--      why the int? (because it's the field number; look at how its used in
--      creating the attributes of the div)
--
--      why the string? (it's the value attribute)
--
--   Then convert a list of fields to a list of tuples (<index>, <field
--   value>) by calling my function withIndices below.
--
--    and then map this function over the list of strings in the model.
--
--  My own answer is in the next file, several-fields-04.elm.



import Browser
import Html exposing (Html, text, div, input)
import Html.Attributes exposing (type_, value)
import Html.Events exposing (onInput, onClick)

----------------------------------------------------------------------
----------------------------------------------------------------------
--     The code between these lines is what you'll need to do
--     several-fields-04.elm

-- HERE'S MY DEFINITION of updateElem. Notice it uses 'withIndices' below.
updateElem : Int -> a -> List a -> List a
updateElem n x l =
  let
    combine : (Int,a) -> a
    combine (n2, x2) =
      if n2 == n
        then x
        else x2
  in
    List.map combine (withIndices l)

-- convert a list of elements to a list of tuples in which each element is
-- associated with its index.
--
-- for instance, the list ["bob", "george", "fred"] becomes
-- [ (0, "bob"), (1, "george"), (2, "fred") ]
withIndices : List a -> List (Int, a)
withIndices l =
    List.map2 Tuple.pair (List.range 0 (List.length l - 1)) l

----------------------------------------------------------------------
----------------------------------------------------------------------



main =
  Browser.sandbox { init = init, update = update, view = view }


type alias Model =
  { fields : List String }


type Msg = Change Int String


init : Model
init = { fields = ["Craig", "Joe", "Bob", "Mary", "Peter", "Paul"] }


-- QUESTION: why is the last case using a hole? Try putting 6 there instead
-- and see what error you get. Write why you think that's happening.
update : Msg -> Model -> Model
update msg model =
  case msg of
      Change i s -> { model | fields = updateElem i s model.fields}




-- QUESTION: why are the arguments to 'onInput' defined this way? How does
-- that relate to the idea that an argument to 'onInput' has to be a function
-- that takes a string?
view : Model -> Html Msg
view model =
  let
    listIndices : List Int
    listIndices = List.range 0 (List.length model.fields - 1)
    inputFields2 = List.map2 test01 listIndices model.fields
  in
    div [] inputFields2

createDiv : String -> (String -> Msg) -> Html Msg
createDiv mo func =
  input [ type_ "text", value mo, onInput func] []

test01 : Int -> String -> Html Msg
test01 i s = input [ type_ "text", value s, onInput (Change i)] []
