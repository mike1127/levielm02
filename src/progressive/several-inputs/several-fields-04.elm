-- Assignment 3:
--
-- 1. Make sure you understand each part of this file and what it does. Write
-- a few sentences about the following:
--
--   a. What appears to be what this program does? (run it)
--   aa. What are any new elements compared to the last exercise?
--   b. What is the Model defined this way? Why have these fields?
--   c. Why is 'init' defined that way?
--   d. Why is update defined this way?
--   e. what is the structure of the web page created by 'view'?
--
-- At this point I need to make some more exercises, let me know if you get
-- here.


import Browser
import Html exposing (Html, text, div, input)
import Html.Attributes exposing (type_, value)
import Html.Events exposing (onInput, onClick)


-- HERE'S MY DEFINITION of updateElem. Notice it uses 'withIndices' below.
updateElem : Int -> a -> List a -> List a
updateElem n x l =
  let
    combine : (Int,a) -> a
    combine (n2, x2) =
      if n2 == n
        then x
        else x2
  in
    List.map combine (withIndices l)


withIndices : List a -> List (Int, a)
withIndices l =
    List.map2 Tuple.pair (List.range 0 (List.length l - 1)) l


main =
  Browser.sandbox { init = init, update = update, view = view }


type alias Model =
  { fields      : List String
  }


type Msg = Change Int String


init : Model
init = { fields = [ "Craig", "Joe", "Bob", "Mary", "Peter", "Paul" ] }


-- QUESTION: why is the last case using a hole? Try putting 6 there instead
-- and see what error you get. Write why you think that's happening.
update : Msg -> Model -> Model
update msg model =
  case msg of
      Change n s -> { model | fields = updateElem n s model.fields }


makeDiv : (Int, String) -> Html Msg
makeDiv (n, s) = input [ type_ "text", value s, onInput (Change n) ] []


-- QUESTION: why are the arguments to 'onInput' defined this way? How does
-- that relate to the idea that an argument to 'onInput' has to be a function
-- that takes a string?
view : Model -> Html Msg
view model =
  div [] (List.map makeDiv <| withIndices model.fields)
