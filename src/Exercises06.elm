

-- Exercises in function composition

import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)


main =
  Html.text <| Debug.toString output


----------------------------------------------------------------------
--    Vehicle


-- output = getOccupants testVehicle1
{-
-- all occupandts in list of test vehicles
output =
  testVehicleList
    |> List.map getOccupants 
    |> List.concat
-}


type Vehicle = Car String (List String)  -- brand of car, names of occupants
             | Truck String (List String)


testVehicle1 = Car   "Honda"     ["George", "Fred"]
testVehicle2 = Truck "Toyota"    ["Mary"]
testVehicle3 = Truck "Navigator" ["kid 1", "kid 2", "kid 3"]
testVehicle4 = Car   "Google"    []


testVehicleList = [testVehicle1, testVehicle2, testVehicle3, testVehicle4]


getOccupants : Vehicle -> List String
getOccupants veh = case veh of
  Car _ occupants   -> occupants
  Truck _ occupants -> occupants


getMake : Vehicle -> String
getMake veh = case veh of
  Car   make _ -> make
  Truck make _ -> make


isInVehicle : String -> Vehicle -> Bool
isInVehicle name veh = 
  veh
    |> getOccupants
    |> List.member name


-- output =  inVehicle "George" testVehicle1


----------------------------------------------------------------------
--             Address


type alias Name = String


type alias Street = String


type Address = StreetAddress Name Int Street
             | POBoxAddress Name Int


getAddressName addr = 
  case addr of
    StreetAddress name _ _ -> name
    POBoxAddress name _    -> name


testAddress1 = StreetAddress "Bob" 999 "Excelsior"
testAddress2 = POBoxAddress "Jonny" 1


testAddresses : List Address
testAddresses = [ StreetAddress "Paul" 460 "Oakland"
                , POBoxAddress "Mike" 94611
                , POBoxAddress "Brenda" 12345
                , StreetAddress "Fred" 111 "Jewel" ]


-- output = List.any (getAddressName >> ((==) "Paul")) testAddresses
output = List.all longName testAddresses


longName : Address -> Bool
longName addr = 
  addr
    |> getAddressName
    |> \n -> String.length n > 4




----------------------------------------------------------------------
--             List.all, List.any, List.sum


