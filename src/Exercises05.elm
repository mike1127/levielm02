
-- Exercises in function composition

import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)

main =
  Html.text <| Debug.toString output

----------------------------------------------------------------------
--
-- Write a function that reverses a list of int, drops two items, and squares
-- each number using function composition
--
-- Let's take this one at a time. 

-- A function that reverses a list
func01 : List Int -> List Int
func01 l = List.reverse l

-- A function that drops two items.
func02 : List Int -> List Int
func02 l = List.drop 2 l

-- A function that drops multiplies by two
func03 : List Int -> List Int
func03 l = List.map (\x -> 2 * x) l


-- We can combine them like this:
--
-- this is just like writing h(g(f(x))) in math.

combinedA l = func03 (func02 (func01 l))

testData = [1, 2, 3, 4, 5, 6, 7]

-- uncomment this:
-- output = combinedA testData


-- We can also eleminate the parentheses:

combinedB l = func03 <| func02 <| func01 l

-- Now, in math we can write the composition of f, g, and h as
--              h . g . f
--
-- Notice how there's no argument in math. You don't write
--
--          h . g . f(x)   <--- no

-- In a functional language you can write a composition of func01, func02, and
-- func03 as

combinedC = func03 << func02 << func01

-- go ahead and try it. uncomment this line!

-- output = combinedC testData

-- Note that we can write func01, func02, func03 by dropping the argument:

func01_B = List.reverse

func02_B = List.drop 2

func03_B = List.map (\x -> x * 2)

-- So in the end you can write

combinedD = List.map (\x -> x * 2) << List.drop 2 << List.reverse

-- output = combinedD testData


-- THIS IS THE EXERCISE
--
-- Now you try it. Compose three functions operating on a list: Multiply each
-- element by 2, square each element, and filter out any number divisible by
-- 4.

-- Here are the functions. Create test data for each one.

func04 l = List.map (\x -> 2 * x) l
func05 l = List.map (\x -> x * x) l
func06 l = List.filter (\x -> x % 4 /= 0) l


-- Then rewrite them without arguments, and use that to suggest how to build a
-- composition of all three. Test your composition.


