import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Random exposing (Generator)



-- MAIN


main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = \_ -> Sub.none
    , view = view
    }



-- MODEL


type alias Model =
  { dieFace : (Int, Int)
  }


init : () -> (Model, Cmd Msg)
init _ =
  ( Model (2, 2)
  , Cmd.none
  )



-- UPDATE

-- generate : (a -> Msg) -> Generator a -> Cmd Msg

type Msg
  = Roll
  | NewFace (Int, Int)

generator1to3 : Generator Int
generator1to3 = Random.int 1 3

generator4to6 : Generator Int
generator4to6 = Random.int 4 6

generatorPair : Generator (Int, Int)
generatorPair = Random.pair generator1to3 generator4to6

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Roll ->
      ( model
      , Random.generate NewFace generatorPair
      )

    NewFace p ->
      ( Model p
      , Cmd.none
      )



-- SUBSCRIPTIONS

{-

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

-}

-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ h1 [] [ text (Debug.toString model.dieFace) ]
    , button [ onClick Roll ] [ text "Roll" ]
    ]
