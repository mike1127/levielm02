import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Random



-- MAIN


main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }



-- MODEL


type alias Model =
  { numberGuess : Int
    generatedNumber : Int
  }


init : () -> (Model, Cmd Msg)
init _ =
  ( Model 0
  , Cmd.none
  )



-- UPDATE


type Msg
  = Roll
  | NewFace Int


update : Msg -> Model -> Int -> Model
update msg model number =
  case msg of





-- SUBSCRIPTIONS

{-
subscriptions : Model -> Sub Msg
subscriptions model =
Sub.none
-}



-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ h1 [] [ text (String.fromInt model.dieFace) ]
    , button [ onClick Roll ] [ text "Roll" ]
    ]
