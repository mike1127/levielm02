import random as r


def main():
    generatedNumber = r.randint(1, 10)
    while True:
        inp = int(input("Enter in a number: "))
        print(compare(generatedNumber, inp))


def compare(gen, inp):
    if gen == inp:
        return "Correct!"
    if gen < inp:
        return "A little lower..."
    if gen > inp:
        return "A little higher"

main()
