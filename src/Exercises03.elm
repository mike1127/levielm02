
-- Levi, in the following file there are lots of commented-out lines that
-- define 'output'. Uncomment each one and test it. Some of them are just
-- examples I'm giving to you (which you should still test), while some
-- require that you solve the problem yourself.

import Browser
import Debug
import Html exposing (Html, Attribute, div, input, text, span, button)

----------------------------------------------------------------------

-- TODO
-- TODO: makes examples with List.concat
-- TODO


-- what do I need to teach? polymorphic, higher order functions, partial
-- function application

-- how to teach higher order functions? what types have parameters?
-- list. polymorphic. tuple

-- elm run

----------------------------------------------------------------------
-- helpful function

main =
  Html.text <| Debug.toString output


----------------------------------------------------------------------
-- Exercise 00A

-- keep the tuples that have a True and concatenate their strings.

test00A_1 = [(False, "bob"), (True, "fred"), (True, "peter"), (False, "mary")]

----------------------------------------------------------------------
-- Exercise 00B

test00B_1 = [1, 2, 3, 4, 5]
test00B_2 = [6, 7, 8, 9, 10]

-- multiply the lists together and  keep the products over 20


----------------------------------------------------------------------
-- Exercise 00C

-- Given an int, find all its divisors
--
f_00C : Int -> List Int
f_00C = Debug.crash

----------------------------------------------------------------------
-- Exercise 01
--
-- 
-- The goal is to write a function that operates on lists without knowing what
-- type the list contains. Here are some operations you should know:
--
-- List.reverse : reverse contents of a list
--
-- List.drop <n> : drop the first <n> elements of a list. Unlike List.tail,
-- this always returns a list type and is safe for any size list.

-- examples of list.drop

-- output = List.drop 2 [2, 3, 4, 5]
-- output = List.drop 10 [2, 3, 4, 5]

-- Your job is to write a function that removes the **last** two elements of a
-- list and yields the new list. Let's call it 'dropLast2'

-- First of all, what's the type signature? Will it specify a specifc type for
-- the contents of the list or will it leave it open or unknown?
-- 
-- dropLast2 : <type signature here>
--
-- test with the following data
-- 
-- test1 = [1, 2, 3, 4, 5]
-- test2 = ["foo", "bar"]
-- test3 = [1]

dropLast2 : List a -> List a
dropLast2 x = List.reverse <| List.drop 2 <| List.reverse x

-- output = dropLast2 test3


----------------------------------------------------------------------
-- Exercise 02
--
-- Partial function application. 
--
-- Write a function that takes a list of strings, and maps to
-- a new list in which each string is repeated 3 times.

test02_1 = ["Fred", "Mary", "beef", "book"]

repeatStringList : List String -> List String
repeatStringList l = List.map (\s -> String.repeat 3 s) l

-- output = repeatStringList test02_1


-- String.repeat is a function of two arguments. 
--   String.repeat : Int -> String -> String
--
-- If you take a function like this and provide it with only one argument, it
-- doesn't cause a syntax error.
--
-- f = String.repeat 4   -- ??? what does this function f do?
--
-- it has the type signature
-- f : String -> String
--
-- It takes a string and repeats it three times!

repeatStringListV2 : List String -> List String
repeatStringListV2 l = List.map (String.repeat 4) l

-- output = repeatStringListV2 test1


----------------------------------------------------------------------
-- You can even drop the 'l' in the definition of repeatStringListV2 above.

repeatStringListV3 = List.map (String.repeat 4)

-- output = repeatStringListV3 ["mary", "jane", "bob"]


----------------------------------------------------------------------
-- Exercise: write a function that drops the left 2 characters of every string
-- in a list of Strings. Use partial application as much as you can.

dropEveryLeft2 = List.map (String.dropLeft 2)

-- output = dropEveryLeft2 ["foo", "bar", "sun", "bob"]


----------------------------------------------------------------------
--
-- Function composition
--
-- In math, if you have two functions f(x) and g(x), you might like to compute
-- f(g(x)). That is first you plug x into g, then plug the result into f.
--
-- There's a shortcut for writing this combined function, called the
-- "composition operator". It's a small circle:   f o g
--

f n = n * n

g s = String.length s

combined : String -> Int
combined s = f (g s)

combinedV2 = f << g

-- output = combinedV2 "foo"


----------------------------------------------------------------------
--
-- Write a function that reverses a list, drops two items, and maps a function
-- over it

reverseDrop2Reverse : List Int -> List Int
reverseDrop2Reverse = List.map (\x -> x * x) << List.drop 2 << List.reverse

-- output = reverseDrop2Reverse [1, 2, 3, 4, 5]



