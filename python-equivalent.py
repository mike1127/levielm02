
# ----------------------------------------------------------------------
#
# The "maybe" example (Learn03.elm)


def maybeNumber(n):
    if n % 2 == 0:
        return n/2
    return None

def maybeExample():
    print( maybeNumber( 8 ) )
    print( maybeNumber( 7 ) )

# ----------------------------------------------------------------------
#
# The list example (Learn04.elm)

def tail(lst):
    if len(lst) >= 1:
        return lst[1:]
    return []

def listExample():
    xs = [1, 2]
    print( tail( xs ) )
    print( tail( tail ( xs ) ) )
    print( tail( tail( tail ( xs ) ) ) )
    

# ----------------------------------------------------------------------
#
# the 'Maybe List' example (Learn05.elm)
#
# a variable of type Maybe List is either None or a list.
#
# None in Python corresponds to Nothing in Elm
# a list <list> in Python corresponds to Just <list> in Elm

def maybeTail(lst):
    if lst is None:
        return None
    if len(lst) == 0:
        return None
    return lst[1:]

def maybeListExample():
    xs = [1, 2]
    print( maybeTail( xs ) )
    print( maybeTail( maybeTail( xs ) ) )
    print( maybeTail( maybeTail( maybeTail( xs ) ) ) )


# ----------------------------------------------------------------------
#
#   The Vehicle example (Learn06.elm)

vehicle1 = ("truck", None)
vehicle2 = ("car"  , 1000000.0)
vehicle3 = ("boat" , 70000.0)

def showVehicle(v):
    return "type: {}, price: {} ".format(v[0], v[1])

def vehicleExample():
    print(showVehicle(vehicle1))

    
# ----------------------------------------------------------------------
    
maybeListExample()
